/*==================[inclusions]=============================================*/
#include "main.h"
#include "board.h"
#include "math.h"
#include "adc_api.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*breve inicializacion y configuracion del adc*/
void initADC(void)
{
	/*config adc*/
	ADC_CLOCK_SETUP_T adc_setup;

	// Setup ADC0 with the default values: 10-bit, 400kSPS
	Chip_ADC_Init(LPC_ADC0, &adc_setup);  // [UM:47.6.1]

	// Enable ADC0 CH1
	Chip_ADC_EnableChannel(LPC_ADC0, ADC_CH1, ENABLE);  // [UM:47.6.1]
}


/*esta funcion retorna el valor cuadratico del valor de corriente que muestrea el adc*/
float GetPowCorr (void) {
	uint16_t adc_value;
	float corr=0;
	float voltaje_sensor=0;


	Chip_ADC_SetStartMode(LPC_ADC0, ADC_START_NOW, 0);  // [UM:47.6.1]
	/* When the A/D conversion ends read its value */
		if (Chip_ADC_ReadStatus(LPC_ADC0, ADC_CH1, ADC_DR_DONE_STAT) == SET) {
		Chip_ADC_ReadValue(LPC_ADC0, ADC_CH1, &adc_value);  // [UM:47.6.4]

		/*voltaje_sensor = (adc_value * Vmax_adc) / (2^bits_del_adc-1) */
		voltaje_sensor=(adc_value*3.3)/1023;

		/*corriente = (voltaje_sensor * Imax_pinza) / Vmax_pin1za */
		corr=(voltaje_sensor*10)/3.3;

		/*retorna la muestra de corriente al cuadrado*/
		corr=powf(corr,2);

		return (corr);
	}
	return (-1);
}

/*esta funcion retorna el valor eficaz de corriente dada la suma de muestras de corriente al cuadrado*/
float GetRMSCorr (float corr) {

	/*se divide la sumatoria de muestras al cuadrado por la cantidad de muestras*/
	corr=corr/500;

	/*se saca la raiz cuadrada de la sumatoria
	dividida por el numero de muestras para obtener el valor eficaz de la corriente*/
	corr=sqrtf(corr);

	return (corr);
}


/*==================[external functions definition]==========================*/

/*==================[end of file]============================================*/
