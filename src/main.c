/*==================[inclusions]=============================================*/
#include "main.h"

#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "board.h"

#include "task.h"
#include "semphr.h"

#include "ciaaUART.h"
#include "math.h"
#include "adc_api.h"


/*==================[macros and definitions]=================================*/
#define MSG_SIZE 256

/*==================[internal data declaration]==============================*/

typedef struct TASK_DATA {
	bool flag;
	float amp;
} TASK_DATA_T;

/*==================[internal functions declaration]=========================*/

static void initHardware(void);

/*==================[internal data definition]===============================*/
static TASK_DATA_T	dato;
static uint16_t adc_value;
static uint16_t i=0;

static 	float amp_ant=-1;
static 	float corr=0;
static 	float voltaje_sensor=0;
static 	float sumatoria=0;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void initHardware(void)
{
	Board_Init();
	SystemCoreClockUpdate();
	ciaaUARTInit();
	Chip_RTC_Init(LPC_RTC);
	initADC();
}

static void initTasksData(void)
{
	dato.amp=0;
	dato.flag=false;
}



void task_uart (void *data)
{
	TASK_DATA_T *pdato = (TASK_DATA_T *)data;
	char buffer[MSG_SIZE];

	while (1) {
		if (pdato->flag==true){
			sprintf(buffer, "corr_amp: %.3f\r\n", pdato->amp);
			Chip_UART_SendBlocking(LPC_USART2, buffer, strlen(buffer));
			pdato->flag=false;
		}
	}
}

void task_led (void *data)
{
	while (1) {
		Board_LED_Toggle(0);
		vTaskDelay(300);
	}
}

void task_adc (void *data)
{
	TASK_DATA_T *pdato = (TASK_DATA_T *)data;
	while (1) {
		/*corriente eficaz de 500 ms/25 ciclos*/
		for (i = 0; i < 500; i++) {
			sumatoria=sumatoria+GetPowCorr();
			/*se toma una muestra cada 1ms aprox*/
			vTaskDelay(1);
		}
		pdato->amp=GetRMSCorr(sumatoria);

		if (amp_ant<0){
			amp_ant=pdato->amp;
			pdato->flag=true;
		}
		/*solo si el dato tiene una diferencia mayor a 10 mA se registra un cambio*/
		if ( ( ((pdato->amp)>(amp_ant+0.01)) | ((pdato->amp)<(amp_ant-0.01)) ) ){
			pdato->flag=true;
			amp_ant=pdato->amp;
		}
		sumatoria=0;
		i=0;
	}
}


/*==================[external functions definition]==========================*/

int main(void)
{
	initHardware();

	initTasksData();

	xTaskCreate(task_uart, (const char *)"taskUART",
			1024, &dato, tskIDLE_PRIORITY+1, NULL);

	xTaskCreate(task_adc, (const char *)"taskadc",
			1024, &dato, tskIDLE_PRIORITY+1, NULL);


	xTaskCreate(task_led, (const char *)"taskled",
			configMINIMAL_STACK_SIZE*2, NULL, tskIDLE_PRIORITY+1, 0);

	vTaskStartScheduler();

	while (1) {
	}
}

/*==================[end of file]============================================*/
