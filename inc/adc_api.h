/*
 * SD_API.h
 *
 *  Created on: 22 jun. 2019
 *      Author: german
 */

#ifndef _ADC_API_H_
#define _ADC_API_H_

/*==================[inclusions]=============================================*/

#include "board.h"
#include "math.h"
#include <stdint.h> /* int16_t, uint8_t, etc. */

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/*breve inicializacion y configuracion del adc*/
void initADC(void);


/*esta funcion retorna el valor cuadratico del valor de corriente que muestrea el adc*/
float GetPowCorr (void);


/*esta funcion retorna el valor eficaz de corriente dada la suma de muestras de corriente al cuadrado*/
float GetRMSCorr (float);

/*==================[end of file]============================================*/

#endif /* PROJECTS_TP_FINAL_INC_SD_API_H_ */
